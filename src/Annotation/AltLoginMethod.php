<?php

namespace Drupal\alt_login\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an alternative to logging in with the username.
 *
 * @Annotation
 */
class AltLoginMethod extends Plugin {

  public $id;

  public $label;

  public $description;

  public $weight;

}
